resource "google_compute_instance" "binar_demo" {
  count = 1
  name = "binar-demo-terraform-${count.index+1}"
  machine_type = "e2-small"
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2204-lts"
    }
  }
  metadata = {
    ssh-keys = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDKS5nPLrpee9g83Pmd/09hm4pI2fmWq8JrkdP79VZu/BolPVeTR1pyNWsm+Hmya2jN1eC9Jw9z6n4QmeoaZKtH3pPTbWbWZckeynHskMLwVwS+wL/UbSXxCtGhGAUeijGO0h4Rd3rA2EXb13FxQeGCNqTYN/7Pnrn7QWumFlR+PofahBfS3CqCh/KDov/+vyOekjS8bw/+Dv5Hp8g9hG4BHQkXuUUCUVXHCipAundmKlC5b/7NzRsubZMV4inWOAQvc54r0H9T0Z/ySdwUdomClkrIvAndDF/e+0DBDxrTqAjjvDKQKTEuZU1E3FSYsOGByzfMlkfccliVO+gJPTjrT6CFCP8Bt9g4VyPAXEOPQ6j3kUivBsiWcWM8XjRlMMcpsmXa3ZyINpp/uScPx0fUIrtFPwY+ydFjrN3JodIb3eRCzOc7iG4DF3uGsSvKd6NOfwQWK9tBfhUS9H+DEP2f+8Lj8EEKYTW7DsJd802C5v3lJiT4WnmwlaS+AJlB/zs= yanfei@SH67H"
  }
  tags = ["http-server", "https-server"]
  network_interface {
    network = "default"

    access_config {
      // Ephemeral public IP
    }
  }
}