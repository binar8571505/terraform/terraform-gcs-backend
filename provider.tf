terraform {
  backend "gcs" {
    bucket = "binar-tf-state-syaifuddin"
    prefix = "staging"
  }
}
provider "google" {
  project     = "analog-medium-374702"
  region      = "asia-southeast2"
  zone        = "asia-southeast2-a"
}